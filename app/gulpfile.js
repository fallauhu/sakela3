var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var templateCache = require('gulp-angular-templatecache');
var del = require('del');

var paths = {
  sass: ['./scss/**/*.scss'],
  templatecache: ['./www/templates/**/*.html']
};

gulp.task('default', ['sass', 'templatecache']);

gulp.task('templatecache', function (done) {
  gulp.src('./www/templates/**/*.html')
    .pipe(templateCache({standalone:true}))
    .pipe(gulp.dest('./www/js'))
    .on('end', done);
});

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.templatecache, ['templatecache']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('delete-files', function() {
 //remove unnecessary files
 console.log('delete-files STARTED');
 del([

   'platforms/android/assets/www/lib/angular-animate/**',
   'platforms/android/assets/www/lib/angular-sanitize/**',
   'platforms/android/assets/www/lib/angular-ui-router/**',
   'platforms/android/assets/www/lib/angular/**',

   'platforms/android/assets/www/lib/ionic/**',
   '!platforms/android/assets/www/lib/ionic',
   '!platforms/android/assets/www/lib/ionic/fonts',
   '!platforms/android/assets/www/lib/ionic/fonts/*',
   '!platforms/android/assets/www/lib/ionic/js',
   '!platforms/android/assets/www/lib/ionic/js/ionic.bundle.js',

   'platforms/android/assets/www/lib/pouchdb/**',
   '!platforms/android/assets/www/lib/pouchdb',
   '!platforms/android/assets/www/lib/pouchdb/dist',
   '!platforms/android/assets/www/lib/pouchdb/dist/pouchdb.js',

   'platforms/android/assets/www/lib/angular-pouchdb/**',
   '!platforms/android/assets/www/lib/angular-pouchdb',
   '!platforms/android/assets/www/lib/angular-pouchdb/angular-pouchdb.js',

   'platforms/android/assets/www/lib/ng-lodash/**',
   '!platforms/android/assets/www/lib/ng-lodash',
   '!platforms/android/assets/www/lib/ng-lodash/build',
   '!platforms/android/assets/www/lib/ng-lodash/build/ng-lodash.js',

   'platforms/android/assets/www/lib/ngCordova/**',
   '!platforms/android/assets/www/lib/ngCordova',
   '!platforms/android/assets/www/lib/ngCordova/dist',
   '!platforms/android/assets/www/lib/ngCordova/dist/ng-cordova.js',

   'platforms/android/assets/www/lib/angular-svg-round-progressbar/**',
   '!platforms/android/assets/www/lib/angular-svg-round-progressbar',
   '!platforms/android/assets/www/lib/angular-svg-round-progressbar/build',
   '!platforms/android/assets/www/lib/angular-svg-round-progressbar/build/roundProgress.js',

   'platforms/android/assets/www/lib/ion-sticky/**',
   '!platforms/android/assets/www/lib/ion-sticky',
   '!platforms/android/assets/www/lib/ion-sticky/ion-sticky.js',

   'platforms/android/assets/www/lib/ng-cordova-oauth/**',
   '!platforms/android/assets/www/lib/ng-cordova-oauth',
   '!platforms/android/assets/www/lib/ng-cordova-oauth/dist',
   '!platforms/android/assets/www/lib/ng-cordova-oauth/dist/ng-cordova-oauth.js',

   'platforms/android/assets/www/lib/angular-translate/**',
   '!platforms/android/assets/www/lib/angular-translate',
   '!platforms/android/assets/www/lib/angular-translate/angular-translate.js',

   'platforms/android/assets/www/templates/**',

   'platforms/ios/www/lib/angular-animate/**',
   'platforms/ios/www/lib/angular-sanitize/**',
   'platforms/ios/www/lib/angular-ui-router/**',
   'platforms/ios/www/lib/angular/**',

   'platforms/ios/www/lib/ionic/**',
   '!platforms/ios/www/lib/ionic',
   '!platforms/ios/www/lib/ionic/fonts',
   '!platforms/ios/www/lib/ionic/fonts/*',
   '!platforms/ios/www/lib/ionic/js',
   '!platforms/ios/www/lib/ionic/js/ionic.bundle.js',

   'platforms/ios/www/lib/pouchdb/**',
   '!platforms/ios/www/lib/pouchdb',
   '!platforms/ios/www/lib/pouchdb/dist',
   '!platforms/ios/www/lib/pouchdb/dist/pouchdb.js',

   'platforms/ios/www/lib/angular-pouchdb/**',
   '!platforms/ios/www/lib/angular-pouchdb',
   '!platforms/ios/www/lib/angular-pouchdb/angular-pouchdb.js',

   'platforms/ios/www/lib/moment/**',
   '!platforms/ios/www/lib/moment',
   '!platforms/ios/www/lib/moment/moment.js',

   '!platforms/ios/www/lib/moment/locale',
   '!platforms/ios/www/lib/moment/locale/es.js',

   'platforms/ios/www/lib/angular-moment/**',
   '!platforms/ios/www/lib/angular-moment',
   '!platforms/ios/www/lib/angular-moment/angular-moment.js',

   'platforms/ios/www/lib/ng-lodash/**',
   '!platforms/ios/www/lib/ng-lodash',
   '!platforms/ios/www/lib/ng-lodash/build',
   '!platforms/ios/www/lib/ng-lodash/build/ng-lodash.js',

   'platforms/ios/www/lib/ngCordova/**',
   '!platforms/ios/www/lib/ngCordova',
   '!platforms/ios/www/lib/ngCordova/dist',
   '!platforms/ios/www/lib/ngCordova/dist/ng-cordova.js',

   'platforms/ios/www/lib/angular-svg-round-progressbar/**',
   '!platforms/ios/www/lib/angular-svg-round-progressbar',
   '!platforms/ios/www/lib/angular-svg-round-progressbar/build',
   '!platforms/ios/www/lib/angular-svg-round-progressbar/build/roundProgress.js',

   'platforms/ios/www/lib/ion-sticky/**',
   '!platforms/ios/www/lib/ion-sticky',
   '!platforms/ios/www/lib/ion-sticky/ion-sticky.js',

   'platforms/ios/www/lib/ng-cordova-oauth/**',
   '!platforms/ios/www/lib/ng-cordova-oauth',
   '!platforms/ios/www/lib/ng-cordova-oauth/dist',
   '!platforms/ios/www/lib/ng-cordova-oauth/dist/ng-cordova-oauth.js',

   'platforms/ios/www/lib/angular-translate/**',
   '!platforms/ios/www/lib/angular-translate',
   '!platforms/ios/www/lib/angular-translate/angular-translate.js',

   'platforms/ios/www/templates/**',

   ])
   .then(function() {
     console.log('delete-files DONE');
   });
});


gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});

gulp.task('serve:before', ['sass', 'templatecache', 'watch']);

