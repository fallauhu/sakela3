angular.module('sakela')

  .controller('InicioCtrl', function($scope, $state, $ionicSideMenuDelegate) {

    var ctrl = this;

    ctrl.login = function() {
      $state.go('app.login');
    };

    $scope.$on('$ionicView.enter', function(){
      $ionicSideMenuDelegate.canDragContent(false);
    });

    $scope.$on('$ionicView.leave', function(){
      $ionicSideMenuDelegate.canDragContent(true);
    });

  });

