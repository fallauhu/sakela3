angular.module('sakela')

  .controller('FavoritosCtrl', function(
    $log,
    $q,
    $scope,
    $state,
    $timeout,
    $ionicNavBarDelegate,
    $translate,
    lodash,
    API,
    Diccionario,
    Favoritos
  ) {

    var ctrl = this,
        urls;

    ctrl.seccion = localStorage.diccionario || 'EU_ES';

    urls = {
      EU_ES: {
        euskaltzaindia: 'http://www.euskaltzaindia.eus/index.php?sarrera=##TERMINO##&option=com_hiztegianbilatu&view=frontpage&Itemid=410&lang=eu&bila=bai&device=iphone',
        elhuyar: 'http://hiztegiak.elhuyar.eus/eu_es/##TERMINO##',
        labayru: 'http://hiztegia.bilbozaharra.eus/labayru.php?termino=##TERMINO##'
      },
      ES_EU: {
        euskaltzaindia: '',
        elhuyar: 'http://hiztegiak.elhuyar.eus/es_eu/##TERMINO##',
        labayru: 'http://hiztegia.bilbozaharra.eus/labayru.php?termino=##TERMINO##&lang=es'
      },

    };

    ctrl.terminos = {
      EU_ES: [],
      ES_EU: []
    };

    ctrl.pulsado = {};
    ctrl.pagina = '';
    ctrl.url = '';

    ctrl.cambiarSeccion = function(s) {
      ctrl.seccion = s;
    };

    ctrl.updatePulsado = function(termino) {

      if (termino.id === ctrl.pulsado.id) {
        termino.p = termino.p ? 0 : 1;
      }
      else {

        if (ctrl.pulsado.p) {
          ctrl.pulsado.p = 0;
        }

        ctrl.pulsado = termino;
        ctrl.pulsado.p = 1;
      }

      ctrl.pagina = '';
      ctrl.url = '';

    };

    ctrl.cargarPagina = function(d, t) {

      ctrl.pagina = d;
      ctrl.url = urls[ctrl.seccion][d].replace('##TERMINO##', t.termino);
    };

    ctrl.updateFavorito = function(e, termino) {

      var defer = $q.defer();

      if (termino.isFavorite) {
        Favoritos.del(termino).then(function() {
          defer.resolve();
        }, function() {
          $log.log(arguments);
        });
      }
      else {

        Favoritos.set(termino).then(function() {
          defer.resolve();
        }, function() {
          $log.log(arguments);
        });
      }

      defer.promise.then(function() {
        termino.isFavorite = termino.isFavorite ? 0 : 1;
      });

      e.stopPropagation();

      getFavoritos();
    };

    function getFavoritos() {

      var terminos = {
        EU_ES: [],
        ES_EU: []
      };

      Favoritos.get().then(function(favoritos) {

        lodash.each(favoritos, function(f) {
          f.isFavorite = 1;
          terminos[(f.origen + '_' +  f.destino).toUpperCase()].push(f);
        });

        ctrl.terminos = terminos;
      });
    }

    $scope.$on('$ionicView.enter', function() {
      $translate('FAVORITOS').then(function(t) {
        $ionicNavBarDelegate.title(t);
      });
      getFavoritos();
    });

    $scope.$on('$ionicView.beforeLeave', function() {
      $ionicNavBarDelegate.title('');
    });
  });
