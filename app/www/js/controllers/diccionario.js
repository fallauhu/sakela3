angular.module('sakela')

  .controller('DiccionarioCtrl', function(
    $log,
    $q,
    $rootScope,
    $scope,
    $state,
    $timeout,
    $ionicNavBarDelegate,
    $translate,
    lodash,
    API,
    Diccionario,
    Favoritos
  ) {

    var ctrl = this,
        dic = localStorage.diccionario || 'EU_ES',
        urls;

    urls = {
      EU_ES: {
        euskaltzaindia: 'http://www.euskaltzaindia.eus/index.php?sarrera=##TERMINO##&option=com_hiztegianbilatu&view=frontpage&Itemid=410&lang=eu&bila=bai&device=iphone',
        elhuyar: 'http://hiztegiak.elhuyar.eus/eu_es/##TERMINO##',
        labayru: 'http://hiztegia.bilbozaharra.eus/labayru.php?termino=##TERMINO##'
      },
      ES_EU: {
        euskaltzaindia: '',
        elhuyar: 'http://hiztegiak.elhuyar.eus/es_eu/##TERMINO##',
        labayru: 'http://hiztegia.bilbozaharra.eus/labayru.php?termino=##TERMINO##&lang=es'
      },

    };

    ctrl.terminos = [];
    ctrl.pulsado = {};
    ctrl.pagina = '';
    ctrl.url = '';

    ctrl.updatePulsado = function(termino) {

      if (termino.id === ctrl.pulsado.id) {
        termino.p = termino.p ? 0 : 1;
      }
      else {

        if (ctrl.pulsado.p) {
          ctrl.pulsado.p = 0;
        }

        ctrl.pulsado = termino;
        ctrl.pulsado.p = 1;
      }

      ctrl.pagina = '';
      ctrl.url = '';

    };

    ctrl.cargarPagina = function(d, t) {

      dic = localStorage.diccionario || 'EU_ES';

      ctrl.pagina = d;
      ctrl.url = urls[dic][d].replace('##TERMINO##', t.termino);
    };

    ctrl.updateFavorito = function(e, termino) {

      var defer = $q.defer();

      if (termino.isFavorite) {
        Favoritos.del(termino).then(function() {
          defer.resolve();
        }, function() {
          $log.log(arguments);
        });
      }
      else {

        Favoritos.set(termino).then(function() {
          defer.resolve();
        }, function() {
          $log.log(arguments);
        });
      }

      defer.promise.then(function() {
        termino.isFavorite = !termino.isFavorite;
      });

      e.stopPropagation();
    };

    $rootScope.$on('Diccionario:terminos', function(e, terminos) {

      if (terminos.length > 0) {

        Favoritos.get().then(function(favoritos) {

          var ids;

          ids = favoritos.map(function(f) {
            return f.id;
          });

          if (ids.length > 0) {

            lodash.each(terminos, function(t, i) {
              terminos[i].isFavorite = (ids.indexOf(t.id) !== -1);
            });
          }

          ctrl.terminos = terminos;

          ctrl.pulsado = {};
          ctrl.pagina = '';
          ctrl.url = '';
        });
      }
      else {
          ctrl.terminos = terminos;

          ctrl.pulsado = {};
          ctrl.pagina = '';
          ctrl.url = '';
      }
    });

    $scope.$on('$ionicView.leave', function() {

      if (ctrl.pulsado.p) {
        ctrl.pulsado.p = 0;
        ctrl.pulsado = {};
      }

      ctrl.pagina = '';
      ctrl.url = '';
    });

  });

