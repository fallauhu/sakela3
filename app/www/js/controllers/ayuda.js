angular.module('sakela')

  .controller('AyudaCtrl', function($log, $scope, $timeout, $translate, $ionicNavBarDelegate, API) {

    var ctrl = this;

    ctrl.contenido = '';

    $scope.$on('$ionicView.enter', function() {

      $translate('AYUDA').then(function(t) {
        $ionicNavBarDelegate.title(t);
      });

      API.getAyuda().then(function(res) {
        ctrl.contenido = res.valor;
      });
    });

    $scope.$on('$ionicView.beforeLeave', function() {
      $ionicNavBarDelegate.title('');
    });

  });

