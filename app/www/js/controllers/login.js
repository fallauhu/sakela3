angular.module('sakela')

  .controller('LoginCtrl', function(
    $http,
    $log,
    $q,
    $rootScope,
    $state,
    $scope,
    $ionicSideMenuDelegate,
    $cordovaOauth,
    API
  ) {

    var ctrl = this;

    ctrl.datos = {
      usuario: '',
      clave: ''
    };

    ctrl.errores = {
      usuario: false,
      clave: false
    };

    ctrl.limpiar = function(campo) {
      ctrl.errores[campo] = false;
    };

    ctrl.inicio = function() {
      $state.go('app.inicio');
    };

    ctrl.showEjercicios = function(res) {

      localStorage.token = res.token;
      localStorage.usuario = res.usuario;
      localStorage.email = res.email;

      sessionStorage.token = res.token;
      sessionStorage.usuario = res.usuario;
      sessionStorage.email = res.email;

      $rootScope.$emit('login');

      $state.go('app.indice.ejercicios');
    };

    ctrl.ingresar = function() {

      if (ctrl.datos.usuario === '') {
        ctrl.errores.usuario = true;
        return;
      }

      if (ctrl.datos.clave === '') {
        ctrl.errores.clave = true;
        return;
      }

      API.login(ctrl.datos).then(function(res) {

        $log.log(res);

        if (res.estado === 'KO') {
          ctrl.errores.usuario = true;
          ctrl.errores.clave = true;
        }
        else {
          ctrl.showEjercicios(res);
        }
      });

    };

    ctrl.restrablecer = function() {

      var defer = $q.defer();

      if (navigator.notification) {
        navigator.notification.prompt('Ingresa tu correo electrónico', function(res) {
          defer.resolve(res.input1);
        }, 'Restablecer contraseña', ['ENVIAR INSTRUCCIONES', 'CANCELAR']);
      }
      else {
        defer.resolve(window.prompt('Ingresa tu correo electrónico'));
      }

      defer.promise.then(function(email) {

        if (email) {

          API.restablecer(email).then(function(res) {

            var mensaje;

            if (res.estado === 'OK') {
              mensaje = 'Te hemos enviado un email con las instruciones para restablecer tu cuenta';
            }
            else {
              mensaje = res.mensaje;
            }

            if (navigator.notification) {
              navigator.notification.alert(mensaje, function() {}, 'Restablecer');
            }
            else {
              window.alert(mensaje);
            }
          });
        }
      });
    };

    ctrl.nuevo = function() {
      $state.go('app.nuevo');
    };

    ctrl.facebook = function() {

      var defer = $q.defer();

      $cordovaOauth.facebook('1135239053234078', ['email', 'public_profile'], {redirect_uri: 'http://localhost/callback'}).then(function(result){

          localStorage.accessToken = result.access_token;

          $http.get('https://graph.facebook.com/v2.2/me', {
            params: {
              access_token: localStorage.accessToken,
              fields: 'id,name,email',
              format: 'json'
            }
          }).then(function(result) {

            API.login({
              usuario: result.data.email,
              clave: result.data.id,
              origen: 'facebook'
            }).then(function(res) {

              if (res.estado === 'KO') {

                API.registro({
                  usuario: result.data.name,
                  email: result.data.email,
                  clave: result.data.id,
                  origen: 'facebook'
                }).then(function(res) {
                  if (res.estado === 'OK') {
                    defer.resolve(res);
                  }
                });
              }
              else {
                defer.resolve(res);
              }

              defer.promise.then(function(res) {
                ctrl.showEjercicios(res);
              });

            });
          });
      },  function(error){
              alert("Error: " + error);
      });
    };

    ctrl.google = function() {

      var defer = $q.defer();

      window.plugins.googleplus.login({
      },
      function (result) {

        API.login({
          usuario: result.email,
          clave: result.userId,
          origen: 'google'
        }).then(function(res) {

          if (res.estado === 'KO') {

            API.registro({
              usuario: result.displayName,
              email: result.email,
              clave: result.userId,
              origen: 'google'
            }).then(function(res) {
              if (res.estado === 'OK') {
                defer.resolve(res);
              }
            });
          }
          else {
            defer.resolve(res);
          }

          defer.promise.then(function(res) {
            ctrl.showEjercicios(res);
          });
        });
      },
      function (msg) {
        alert('error: ' + msg);
      }
      );
    };

    $scope.$on('$ionicView.enter', function(){
      $ionicSideMenuDelegate.canDragContent(false);
    });

    $scope.$on('$ionicView.leave', function(){
      $ionicSideMenuDelegate.canDragContent(true);
    });
  });
