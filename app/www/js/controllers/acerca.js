angular.module('sakela')

  .controller('AcercaCtrl', function($log, $scope, $timeout, $translate, $ionicNavBarDelegate, API) {

    var ctrl = this;

    ctrl.contenido = '';

    $scope.$on('$ionicView.enter', function() {

      $translate('ACERCA').then(function(t) {
        $ionicNavBarDelegate.title(t);
      });

      API.getAcerca().then(function(res) {
        ctrl.contenido = res.valor;
      });
    });

    $scope.$on('$ionicView.beforeLeave', function() {
      $ionicNavBarDelegate.title('');
    });
  });


