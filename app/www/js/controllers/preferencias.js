angular.module('sakela')

  .controller('PreferenciasCtrl', function($scope, $timeout, $translate, $ionicNavBarDelegate) {

    var ctrl = this;

    ctrl.lang = localStorage.lang || 'es';

    ctrl.cambiarLenguaje = function(lang) {
      ctrl.lang = lang;
      localStorage.lang = lang;
      $translate.use(lang);
    };

    $scope.$on('$ionicView.enter', function() {
      $translate('PREFERENCIAS').then(function(t) {
        $ionicNavBarDelegate.title(t);
      });
    });

    $scope.$on('$ionicView.beforeLeave', function() {
      $ionicNavBarDelegate.title('');
    });
  });
