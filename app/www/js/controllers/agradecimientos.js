angular.module('sakela')

  .controller('AgradecimientosCtrl', function($scope, $timeout, $translate, $ionicNavBarDelegate, API) {

    var ctrl = this;

    ctrl.contenido = '';

    $scope.$on('$ionicView.enter', function() {

      $translate('AGRADECIMIENTOS').then(function(t) {
        $ionicNavBarDelegate.title(t);
      });

      API.getAgradecimientos().then(function(res) {
        ctrl.contenido = res.valor;
      });
    });

    $scope.$on('$ionicView.beforeLeave', function() {
      $ionicNavBarDelegate.title('');
    });
  });

