angular.module('sakela')

  .controller('EjerciciosCtrl', function(
    $log,
    $scope,
    $state,
    $timeout,
    $translate,
    lodash,
    $ionicModal,
    $ionicNavBarDelegate,
    Actividad,
    API,
    Diccionario,
    Terminos
  ) {

    var ctrl = this;

    ctrl.seccion = sessionStorage.seccion || 'escribir';
    ctrl.nivel = localStorage.nivel || 'A1';
    ctrl.niveles = [ctrl.nivel];
    ctrl.grupos = [];

    ctrl.cuantos = {};

    ctrl.actividad = {};

    ctrl.increment = 0;

    ctrl.grupo = {};

    ctrl.data = {
      escribir: 'termino',
      seleccionar: 'terminos'
    };

    $ionicModal.fromTemplateUrl('escribir-modal.html', {
      id: 'escribir',
      scope: $scope,
      animation: 'slide-in-right'
    }).then(function(modal) {
      ctrl.escribirModal = modal;
    });

    $ionicModal.fromTemplateUrl('seleccionar-modal.html', {
      id: 'seleccionar',
      scope: $scope,
      animation: 'slide-in-right'
    }).then(function(modal) {
      ctrl.seleccionarModal = modal;
    });

    $ionicModal.fromTemplateUrl('resultados-modal.html', {
      id: 'resultados',
      scope: $scope,
      animation: 'slide-in-right'
    }).then(function(modal) {
      ctrl.resultadosModal = modal;
    });

    ctrl.volver = function(seccion) {
      ctrl[seccion + 'Modal'].hide();
    };

    ctrl.cambiarNivel = function() {
      localStorage.nivel = ctrl.nivel;
      getDatos();
    };

    ctrl.cambiarSeccion = function(seccion) {

      if (seccion !== ctrl.seccion) {
        ctrl.seccion = seccion;
        sessionStorage.seccion = seccion;
      }
    };

    ctrl.mostrarEjercicio = function(grupo) {

      var porcentaje = 0;

      ctrl.grupo = {
        nombre: grupo.nombre,
        cuantos: grupo.cuantos,
        id: grupo.id
      };

      if (ctrl.actividad[ctrl.nivel] && ctrl.actividad[ctrl.nivel][grupo.id]) {
        porcentaje = ctrl.actividad[ctrl.nivel][grupo.id] * 100 / ctrl.cuantos['g' + grupo.id];
      }

      if (porcentaje === 100) {

        ctrl.increment = 100;

        ctrl.resultadosModal.show().then(function() {
          ctrl[ctrl.seccion + 'Modal'].hide();
        });
      }
      else {

        if (ctrl.seccion === 'escribir') {
          ctrl.escribir = {
            estado: '',
            respuesta: ''
          };
        }
        else if (ctrl.seccion === 'seleccionar') {

          ctrl.seleccionar = {
            estado: '',
            respuesta: '',
            posicion: Math.floor(Math.random() * 3),
            terminos: [
              {id: 1, termino: '', traduccion: ''},
              {id: 2, termino: '', traduccion: ''},
              {id: 3, termino: '', traduccion: ''}
            ]
          };
        }

        ctrl[ctrl.seccion + 'Modal'].show().then(function() {

          Terminos.getDatos({
            seccion: ctrl.seccion,
            grupo: ctrl.grupo,
            nivel: ctrl.nivel,
            nuevo: true
          }).then(function(res) {

            var indices = [],
                len;

            if (res.data) {

              ctrl[ctrl.seccion].increment = res.increment;
              ctrl[ctrl.seccion][ctrl.data[ctrl.seccion]] = res.data;

              if (ctrl.seccion === 'seleccionar') {

                lodash.each(ctrl.seleccionar.terminos, function(t, i) {
                  if (t.nivel === ctrl.nivel) {
                    indices.push(i);
                  }
                });

                len = indices.length;

                if (len === 1) {
                  ctrl.seleccionar.posicion = indices[0];
                }
                else if (len === 2) {
                  ctrl.seleccionar.posicion = indices[Math.floor(Math.random() * 2)];
                }
              }
            }
          });
        });
      }
    };

    ctrl.opcion = function(indice) {
      ctrl[ctrl.seccion].respuesta = indice;
    };

    ctrl.noReconocido = function() {

      ctrl[ctrl.seccion].estado = 'no_reconocida';

      API.actividad({
        seccion: ctrl.seccion,
        termino: ctrl[ctrl.seccion].termino,
        grupo: ctrl.grupo,
        nivel: ctrl.nivel,
        resultado: ctrl[ctrl.seccion].estado
      });
    };

    ctrl.comprobar = function() {

      var termino;

      if (ctrl.seccion === 'escribir') {

        if (ctrl[ctrl.seccion].termino.termino.toLowerCase() === ctrl[ctrl.seccion].respuesta.toLowerCase()) {
          ctrl[ctrl.seccion].estado = 'correcta';
        }
        else {
          ctrl[ctrl.seccion].estado = 'incorrecta';
        }

        termino = ctrl[ctrl.seccion].termino;

      }
      else if (ctrl.seccion === 'seleccionar') {

        if (ctrl[ctrl.seccion].respuesta === ctrl[ctrl.seccion].posicion) {
          ctrl[ctrl.seccion].estado = 'correcta';
        }
        else {
          ctrl[ctrl.seccion].estado = 'incorrecta';
        }

        termino = ctrl[ctrl.seccion].terminos[ctrl[ctrl.seccion].posicion];
      }

      API.actividad({
        seccion: ctrl.seccion,
        termino: termino,
        grupo: ctrl.grupo,
        nivel: ctrl.nivel,
        resultado: ctrl[ctrl.seccion].estado
      });
    };

    ctrl.continuar = function() {

      if (ctrl.continuando) {
        return;
      }

      ctrl.continuando = true;

      ctrl[ctrl.seccion].estado = '';
      ctrl[ctrl.seccion].respuesta = '';

      if (ctrl.seccion === 'escribir') {
        ctrl[ctrl.seccion].termino.traduccion = '';
      }
      else if (ctrl.seccion === 'seleccionar') {

        ctrl[ctrl.seccion].posicion =  Math.floor(Math.random() * 3);

        ctrl[ctrl.seccion].terminos = [
          {id: 1, termino: '', traduccion: ''},
          {id: 2, termino: '', traduccion: ''},
          {id: 3, termino: '', traduccion: ''}
        ];
      }

      Terminos.getDatos({
        seccion: ctrl.seccion,
        grupo: ctrl.grupo,
        nivel: ctrl.nivel
      }).then(function(res) {

        var indices = [],
            len;

        ctrl.continuando = false;

        $log.log(res);

        if (res.data) {

          ctrl[ctrl.seccion].increment = res.increment;

          if (ctrl[ctrl.seccion].increment < 100) {
            ctrl[ctrl.seccion][ctrl.data[ctrl.seccion]] = res.data;
          }
          else {
            ctrl.continuando = false;

            ctrl.resultadosModal.show().then(function() {
              ctrl[ctrl.seccion + 'Modal'].hide();
            });
          }
        }

        if (ctrl.seccion === 'seleccionar') {

          lodash.each(ctrl.seleccionar.terminos, function(t, i) {
            if (t.nivel === ctrl.nivel) {
              indices.push(i);
            }
          });

          len = indices.length;

          if (len === 1) {
            ctrl.seleccionar.posicion = indices[0];
          }
          else if (len === 2) {
            ctrl.seleccionar.posicion = indices[Math.floor(Math.random() * 2)];
          }
        }
      });
    };

    function getDatos() {

      API.getGrupos(ctrl.nivel).then(function(grupos) {

        var items = [];

        lodash.each(grupos, function(g, i) {

          ctrl.cuantos['g' + g.id] = g.cuantos;

          g.imagen = 'http://hiztegia.bilbozaharra.eus' + g.imagen;

          items.push(g);

        });

        ctrl.grupos = [items];
      });

      API.getNiveles().then(function(niveles) {

        Actividad.getAprendidasNumero().then(function(res) {

          $log.log(res);

          ctrl.actividad = res.aprendidas;

          lodash.each(niveles, function(n) {
            ctrl.cuantos[n.nombre] = n.cuantos;
          });

          ctrl.increment = 0;

          if (ctrl.actividad[ctrl.nivel]) {

            ctrl.increment = (ctrl.actividad[ctrl.nivel].cuantos * 100 / ctrl.cuantos[ctrl.nivel]).toFixed(0);

            if (isNaN(ctrl.increment)) {
              ctrl.increment = 0;
            }
          }

          ctrl.niveles = niveles;

        });
      });
    }

    $scope.$on('modal.hidden', function() {
      getDatos();
    });

    $scope.$on('$ionicView.enter', function() {

      $ionicNavBarDelegate.title('');

      ctrl.seccion = sessionStorage.seccion || 'escribir';
      ctrl.nivel = localStorage.nivel || 'A1';
      ctrl.niveles = [ctrl.nivel];

      ctrl.cuantos = {};

      ctrl.actividad = {};

      ctrl.increment = 0;

      getDatos();

    });
  });

