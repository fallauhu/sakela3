angular.module('sakela')

  .controller('AppCtrl', function($log, $rootScope, $translate, $ionicNavBarDelegate, Diccionario) {

    var ctrl = this;

    ctrl.usuario = sessionStorage.usuario || '';

    ctrl.share = function() {

      var ios = 'https://itunes.apple.com/es/app/sakela/id1188018028',
          android = 'https://play.google.com/store/apps/details?id=com.poselab.sakela';

      if (window.plugins && window.plugins.socialsharing) {

        if (ionic.Platform.isIOS()) {
          window.plugins.socialsharing.share(null, null, null, ios);
        }
        else if (ionic.Platform.isAndroid()) {
          window.plugins.socialsharing.share(null, null, null, android);
        }
      }
    };

    $rootScope.$on('login', function() {
      ctrl.usuario = sessionStorage.usuario;
    });

  });
