angular.module('sakela')

  .controller('NuevoCtrl', function($log, $state, API) {

    var ctrl = this;

    ctrl.datos = {
      usuario: '',
      email: '',
      clave: ''
    };

    ctrl.errores = {
      usuario: false,
      email: false,
      clave: false
    };

    ctrl.registro = function() {
      $state.go('app.login');
    };

    ctrl.limpiar = function(campo) {
      ctrl.errores[campo] = false;
    };

    ctrl.registrar = function() {

      if (ctrl.datos.usuario === '') {
        ctrl.errores.usuario = true;
        return;
      }

      if (ctrl.datos.email === '') {
        ctrl.errores.email = true;
        return;
      }

      if (ctrl.datos.clave === '') {
        ctrl.errores.clave = true;
        return;
      }

      API.registro(ctrl.datos).then(function(res) {

        $log.log(res);

        if (res.estado === 'KO') {
          ctrl.errores[res.campo] = true;
        }
        else {
          sessionStorage.token = res.token;
          $state.go('app.indice.ejercicios');
        }
      });
    };
  });
