angular.module('sakela')

  .controller('IndiceCtrl', function($log, $rootScope, $scope, $state, $timeout, $ionicHistory, $ionicNavBarDelegate, Diccionario) {

    var ctrl = this;

    ctrl.termino = sessionStorage.termino || '';

    ctrl.titulo = Diccionario.getDiccionario();

    $scope.focus = false;

    ctrl.cambiarDiccionario = function() {
      ctrl.titulo = Diccionario.changeDiccionario();
    };

    ctrl.limpiar = function() {
      ctrl.termino = '';
      Diccionario.setTermino(ctrl.termino);
      $scope.focus = true;
    };

    ctrl.irDiccionario = function() {

      if ($ionicHistory.currentView().stateName !== 'app.indice.diccionario') {
        $state.go('app.indice.diccionario');
      }
    };

    ctrl.buscar = function() {

      Diccionario.setTermino(ctrl.termino);

      if ($ionicHistory.currentView().stateName !== 'app.indice.diccionario') {
        $state.go('app.indice.diccionario');
      }
    };

    ctrl.keyPress = function(e) {
      if (e.keyCode === 13 && window.cordova && window.cordova.plugins.Keyboard) {
        window.cordova.plugins.Keyboard.close();
      }
    };


    $rootScope.$on('Diccionario:change', function(e, terminos) {
      ctrl.termino = sessionStorage.termino || '';
      ctrl.buscar();
    });

    $scope.$on('$ionicView.enter', function() {
      if (ctrl.termino !== '') {
        Diccionario.setTermino(ctrl.termino);
      }
    });

  });

