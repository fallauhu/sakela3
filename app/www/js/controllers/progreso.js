angular.module('sakela')

  .controller('ProgresoCtrl', function(
    $log,
    $q,
    $scope,
    $state,
    $timeout,
    $translate,
    lodash,
    $ionicModal,
    $ionicNavBarDelegate,
    Actividad,
    API,
    Diccionario
  ) {

    var ctrl = this;

    ctrl.seccion = 'aprendidas';
    ctrl.nivel = localStorage.nivel || 'A1';
    ctrl.niveles = [ctrl.nivel];

    ctrl.cuantos = {};

    ctrl.terminos = {};
    ctrl.terminos[ctrl.nivel] = {
      'aprendidas': [],
      'en progreso': [],
      'pendientes': []
    };

    ctrl.colores = {
      'aprendidas': '#45ccce',
      'en progreso': '#f76e1e',
      'pendientes': '#98a0a3'
    };

    ctrl.increment = {
      'aprendidas': 0,
      'en progreso': 0,
      'pendientes': 0
    };

    ctrl.increment_g = {};
    ctrl.actividad_g = {};

    $ionicModal.fromTemplateUrl('progreso-modal.html', {
      id: 'progreso',
      scope: $scope,
      animation: 'slide-in-right'
    }).then(function(modal) {
      ctrl.progresoModal = modal;
    });

    ctrl.mostrarProgreso = function(grupo) {

      var porcentaje = 0;

      ctrl.grupo = {
        nombre: grupo.nombre,
        cuantos: grupo.cuantos,
        id: grupo.id
      };

      if (ctrl.actividad[ctrl.nivel] && ctrl.actividad[ctrl.nivel][grupo.id]) {
        porcentaje = ctrl.actividad[ctrl.nivel][grupo.id] * 100 / ctrl.cuantos['g' + grupo.id];
      }

      ctrl.progresoModal.show();
    };

    ctrl.volver = function() {
      ctrl.progresoModal.hide();
    };

    ctrl.cambiarNivel = function() {
      localStorage.nivel = ctrl.nivel;
      getDatos();
    };

    ctrl.cambiarSeccion = function(seccion) {

      if (seccion !== ctrl.seccion) {
        ctrl.seccion = seccion;
      }
    };

    ctrl.pasarA = function(seccion, termino) {

      if (seccion === 'aprendidas') {
        Actividad.setAprendida(termino);
      }
      else if (seccion === 'en progreso') {
        Actividad.setEnProgreso(termino);
      }
      else if (seccion === 'pendientes') {
        Actividad.setPendiente(termino);
      }

      getDatos();
    };

    function getDatos() {

      var defer = $q.defer();

      ctrl.nivel = localStorage.nivel || 'A1';
      ctrl.niveles = [ctrl.nivel];

      API.getGrupos(ctrl.nivel).then(function(grupos) {

        var items = [];

        lodash.each(grupos, function(g, i) {

          ctrl.cuantos['g' + g.id] = g.cuantos;

          g.imagen = 'http://hiztegia.bilbozaharra.eus' + g.imagen;

          items.push(g);

        });

        ctrl.grupos = [items];

        defer.resolve();
      });

      ctrl.terminos = {};
      ctrl.terminos[ctrl.nivel] = {
        'aprendidas': [],
        'en progreso': [],
        'pendientes': []
      };

      ctrl.terminos_g = {};

      ctrl.cuantos = {};

      ctrl.increment[ctrl.seccion] = 0;

      API.getNiveles().then(function(niveles) {

        $log.log(niveles);

        lodash.each(niveles, function(n) {

          ctrl.terminos[n.nombre] = {
            'aprendidas': [],
            'en progreso': [],
            'pendientes': []
          };

          ctrl.terminos_g[n.nombre] = {
            'aprendidas': {},
            'en progreso': {},
            'pendientes': {}
          };

          ctrl.actividad_g[n.nombre] = {
            'aprendidas': {},
            'en progreso': {},
            'pendientes': {}
          };

          ctrl.cuantos[n.nombre] = n.cuantos;
        });

        Actividad.getProgreso(ctrl.nivel).then(function(res) {

          ctrl.actividad = {
            'aprendidas': res.aprendidas,
            'en progreso': res.progreso_g,
            'pendientes': res.pendientes_g
          };

          lodash.each(res.aprendidas, function(grupos, nivel) {
            ctrl.actividad_g[nivel].aprendidas = grupos;
          });

          lodash.each(res.progreso_g, function(grupos, nivel) {
            ctrl.actividad_g[nivel]['en progreso'] = grupos;
          });

          lodash.each(res.pendientes_g, function(grupos, nivel) {
            ctrl.actividad_g[nivel].pendientes = grupos;
          });

          lodash.each(res.niveles_gg, function(grupos, nivel) {
            ctrl.terminos_g[nivel].aprendidas = grupos;
          });

          lodash.each(res.progreso_gg, function(grupos, nivel) {
            ctrl.terminos_g[nivel]['en progreso'] = grupos;
          });

          lodash.each(res.pendientes_gg, function(grupos, nivel) {
            ctrl.terminos_g[nivel].pendientes = grupos;
          });

          lodash.each(res.niveles, function(terminos, nivel) {
            ctrl.terminos[nivel].aprendidas = terminos;
          });

          lodash.each(res.progreso, function(terminos, nivel) {
            ctrl.terminos[nivel]['en progreso'] = terminos;
          });

          lodash.each(res.pendientes, function(terminos, nivel) {
            ctrl.terminos[nivel].pendientes = terminos;
          });

          lodash.each(ctrl.actividad, function(obj, seccion) {

            ctrl.increment[seccion] = 0;

            if (ctrl.actividad[seccion][ctrl.nivel]) {

              ctrl.increment[seccion] = (ctrl.actividad[seccion][ctrl.nivel].cuantos * 100 / ctrl.cuantos[ctrl.nivel]).toFixed(0);

              if (isNaN(ctrl.increment[seccion])) {
                ctrl.increment[seccion] = 0;
              }

              if (ctrl.grupo) {
                ctrl.increment_g[seccion] = (ctrl.actividad_g[ctrl.nivel][seccion][ctrl.grupo.id] * 100 / ctrl.cuantos['g' + ctrl.grupo.id]).toFixed(0);
              }

            }
          });

          defer.promise.then(function() {

            lodash.each(niveles, function(nivel) {

              ctrl.increment_g[nivel.nombre] = {};

              lodash.each(ctrl.actividad, function(obj, seccion) {

                ctrl.increment_g[nivel.nombre][seccion] = {};

                if (ctrl.actividad_g[nivel.nombre][seccion]) {

                  lodash.each(ctrl.grupos[0], function(grupo) {

                    if (!ctrl.increment_g[nivel.nombre][seccion][grupo.id]) {
                      ctrl.increment_g[nivel.nombre][seccion][grupo.id] = 0;
                    }

                    ctrl.increment_g[nivel.nombre][seccion][grupo.id] = (ctrl.actividad_g[nivel.nombre][seccion][grupo.id] * 100 / ctrl.cuantos['g' + grupo.id]).toFixed(0);

                    if (isNaN(ctrl.increment_g[nivel.nombre][seccion][grupo.id])) {
                      ctrl.increment_g[nivel.nombre][seccion][grupo.id] = 0;
                    }
                  });
                }
              });
            });
          });
        });

        ctrl.niveles = niveles;
      });
    }

    $scope.$on('$ionicView.enter', function() {

      getDatos();

    });
  });
