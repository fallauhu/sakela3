angular.module('sakela')

  .controller('SugerenciasCtrl', function($scope, $timeout, $translate, $ionicNavBarDelegate, API) {

    var ctrl = this;

    ctrl.email = sessionStorage.email || '';
    ctrl.texto = '';

    ctrl.enviar = function() {

      if (ctrl.email !== '' && ctrl.texto !== '') {

        API.sugerencia(ctrl.email, ctrl.texto).then(function(res) {

          if (res.estado === 'OK') {
            if (navigator.notification) {
              navigator.notification.alert('Gracias por tu sugerencia', function() {}, 'Sugerencias');
            }
            else {
              window.alert('Gracias por tu sugerencia');
            }
            ctrl.texto = '';
          }
          else {
            if (navigator.notification) {
              navigator.notification.alert('Ha ocurrido un error, íntentalo más tarde', function() {}, 'Sugerencias');
            }
            else {
              window.alert('Ha ocurrido un error, íntentalo más tarde');
            }
          }
        });
      }
    };

    $scope.$on('$ionicView.enter', function() {
      $translate('SUGERENCIAS').then(function(t) {
        $ionicNavBarDelegate.title(t);
      });
    });

    $scope.$on('$ionicView.beforeLeave', function() {
      $ionicNavBarDelegate.title('');
    });
  });

