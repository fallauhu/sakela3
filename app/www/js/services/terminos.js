angular.module('sakela')

  .service('Terminos', function($log, $q, Actividad, API) {

    var siguiente = {};

    function _getDatos(datos) {

      var defer = $q.defer(),
          salida = {};

      if (datos.nuevo) {
        siguiente = {};
      }

      Actividad.getAprendidasNumero(datos).then(function(res) {

        $log.log(res);

        salida.increment = 0;

        if (res.aprendidas[datos.nivel] && res.aprendidas[datos.nivel][datos.grupo.id]) {
          salida.increment = (res.aprendidas[datos.nivel][datos.grupo.id] * 100 / datos.grupo.cuantos).toFixed(0);
        }

        if (isNaN(salida.increment)) {
          salida.increment = 0;
        }

        if (salida.increment === 100) {
          defer.resolve(salida);
        }

        if (siguiente[datos.seccion]) {
          salida.data = siguiente[datos.seccion];
          defer.resolve(salida);
        }

        API.getTerminos(datos.nivel, datos.grupo.id, datos.seccion, siguiente[datos.seccion]).then(function(data) {

          if (!siguiente[datos.seccion]) {

            salida.data = data;

            defer.resolve(salida);

            API.getTerminos(datos.nivel, datos.grupo.id, datos.seccion, data).then(function(data) {
              siguiente[datos.seccion] = data;
            });
          }
          else {
            siguiente[datos.seccion] = data;
          }
        });
      });

      return defer.promise;
    }


    return {
      getDatos: _getDatos
    };
  });
