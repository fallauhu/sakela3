angular.module('sakela')

  .service('Diccionario', function($http, $log, $q, $rootScope, lodash, pouchDB, API) {

    var diccionarios = ['eu_es', 'es_eu'],
        termino = '',
        buscando = false,
        diccionario = localStorage.diccionario || 'EU_ES',
        dic = {};

    function accentsTidy(s) {
      var r=s.toLowerCase();
      r = r.replace(new RegExp("[àáâãäå]", 'g'),"a");
      r = r.replace(new RegExp("æ", 'g'),"ae");
      r = r.replace(new RegExp("ç", 'g'),"c");
      r = r.replace(new RegExp("[èéêë]", 'g'),"e");
      r = r.replace(new RegExp("[ìíîï]", 'g'),"i");
      r = r.replace(new RegExp("[òóôõö]", 'g'),"o");
      r = r.replace(new RegExp("œ", 'g'),"oe");
      r = r.replace(new RegExp("[ùúûü]", 'g'),"u");
      r = r.replace(new RegExp("[ýÿ]", 'g'),"y");
      return r;
    }

    function _init() {

      lodash.each(diccionarios, function(d) {

        dic[d] = pouchDB(d);

        dic[d].replicate.to('http://localhost:5984/' + d);

        dic[d].get('_local/loaded').then(function (doc) {
          $log.log(doc);
        }).catch(function (err) {

          $log.log(err);

          if (err.name !== 'not_found') {
            throw err;
          }

          $http({
            url: d + '.txt',
            responseType: 'blob'
          }).then(function(res) {

            $log.log('Cargando diccionario [' + d + ']...', (new Date()));

            var navigator = new LineNavigator(res.data),
                campos,
                promises = [],
                key,
                BULK = 1000,
                ind = 0,
                items = [],
                item,
                defer = $q.defer();

            navigator.readSomeLines(0, function linesReadHandler(err, index, lines, isEof, progress) {

                var l = lines.length,
                    i;

                for (i = 0; i < l; i++) {
                    campos = lines[i].split('|');

                    item = {
                      _id: accentsTidy(campos[0]),
                      t: campos[0],
                      tr: campos[1],
                      e: campos[2]
                    };

                    items.push(item);

                    ind += 1;

                    if (ind === BULK) {
                      promises.push(dic[d].bulkDocs(items));
                      items = [];
                      ind = 0;
                    }
                }

                if (isEof) {
                  defer.resolve();
                  return;
                }

                navigator.readSomeLines(index + l, linesReadHandler);
            });

            defer.promise.then(function() {

              $log.log(promises);

              $q.all(promises).then(function() {
                $log.log('Diccionario cargado [' + d + ']', (new Date()));
                dic[d].put({_id: '_local/loaded'});
              }, function() {
                $log.log(arguments);
              });
            });
          });
        });
      });

      /*
      lodash.each(diccionarios, function(d) {

        dic[d] = pouchDB(d);

        dic[d].replicate.to('http://localhost:5984/' + d);

        dic[d].get('_local/loaded').then(function (doc) {
          $log.log(doc);
        }).catch(function (err) {

          $log.log(err);

          if (err.name !== 'not_found') {
            throw err;
          }

          $http.get(d + '.txt').then(function(res) {

            $log.log('Cargando diccionario [' + d + ']...', (new Date()));

            var terminos = res.data.split('\n'),
                campos,
                promises = [],
                key,
                BULK = 1000,
                i = 0,
                items = [],
                item;

            lodash.each(terminos, function(t) {

              campos = t.split('|');

              item = {
                _id: accentsTidy(campos[0]),
                t: campos[0],
                tr: campos[1],
                e: campos[2]
              };

              items.push(item);

              i += 1;

              if (i === BULK) {
                promises.push(dic[d].bulkDocs(items));
                items = [];
                i = 0;
              }
            });

            $log.log(promises);

            $q.all(promises).then(function() {
              $log.log('Diccionario cargado [' + d + ']', (new Date()));
              dic[d].put({_id: '_local/loaded'});
            }, function() {
              $log.log(arguments);
            });
          });
        });
      });
      */
    }

    function _setTermino(t) {

      if (buscando) {
        return;
      }

      if (t === '') {
        sessionStorage.termino = '';
        $rootScope.$emit('Diccionario:terminos', []);
        return;
      }

      buscando = true;

      termino = accentsTidy(t);

      API.buscar(termino).then(function(terminos) {
        buscando = false;
        sessionStorage.termino = termino;
        $rootScope.$emit('Diccionario:terminos', terminos);
      });

      /*
      dic.eu_es.allDocs({
        limit: 100,
        include_docs: true,
        startkey: termino,
        endkey:  termino + '\uffff'
      }).then(function(res) {
        buscando = false;
        $log.log(res);
        $rootScope.$emit('Diccionario:terminos', res.rows);
      });
      */
    }

    function _getDiccionario() {
      return localStorage.diccionario || 'EU_ES';
    }

    function _changeDiccionario() {

      var diccionario = localStorage.diccionario || 'EU_ES';

      diccionario = diccionario === 'EU_ES' ? 'ES_EU' : 'EU_ES';

      localStorage.diccionario = diccionario;

      $rootScope.$emit('Diccionario:change');

      return diccionario;
    }

    return {
      init: _init,
      setTermino: _setTermino,
      getDiccionario: _getDiccionario,
      changeDiccionario: _changeDiccionario
    };
  });
