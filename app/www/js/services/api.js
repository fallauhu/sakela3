angular.module('sakela')

  .service('API', function ($http, $q, lodash) {

    var api;

    api = 'http://hiztegia.bilbozaharra.eus/api.php';

    function _login(datos) {

      var defer = $q.defer();

      $http.post(api, {
        accion: 'login',
        datos: datos
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve({});
      });

      return defer.promise;
    }

    function _registro(datos) {

      var defer = $q.defer();

      $http.post(api, {
        accion: 'registro',
        datos: datos
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve({});
      });

      return defer.promise;
    }

    function _getNiveles() {

      var defer = $q.defer();

      $http.post(api, {
        accion: 'niveles',
        token: sessionStorage.token
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve([]);
      });

      return defer.promise;
    }

    function _getGrupos(nivel) {

      var defer = $q.defer();

      $http.post(api, {
        accion: 'grupos',
        nivel: nivel,
        token: sessionStorage.token
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve([]);
      });

      return defer.promise;
    }

    function _getTerminos(nivel, grupo, seccion, anterior) {

      var defer = $q.defer(),
          ant;

      ant = anterior || [];

      $http.post(api, {
        accion: 'terminos',
        nivel: nivel,
        grupo: grupo,
        seccion: seccion,
        anterior: lodash.isArray(ant) ? ant: [ant],
        token: sessionStorage.token
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve([]);
      });

      return defer.promise;
    }

    function _getAyuda() {

      var defer = $q.defer();

      $http.post(api, {
        accion: 'preferencias',
        preferencia: 'ayuda',
        idioma: localStorage.lang,
        token: sessionStorage.token
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve([]);
      });

      return defer.promise;
    }

    function _getAgradecimientos() {

      var defer = $q.defer();

      $http.post(api, {
        accion: 'preferencias',
        preferencia: 'agradecimientos',
        idioma: localStorage.lang,
        token: sessionStorage.token
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve([]);
      });

      return defer.promise;
    }

    function _getAcerca() {

      var defer = $q.defer();

      $http.post(api, {
        accion: 'preferencias',
        preferencia: 'acerca',
        idioma: localStorage.lang,
        token: sessionStorage.token
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve([]);
      });

      return defer.promise;
    }

    function _buscar(termino) {

      var defer = $q.defer();

      $http.post(api, {
        accion: 'terminos',
        seccion: 'diccionario',
        termino: termino,
        diccionario: localStorage.diccionario || 'EU_ES',
        token: sessionStorage.token
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve([]);
      });

      return defer.promise;
    }

    function _actividad(datos) {

      var defer = $q.defer();

      $http.post(api, {
        accion: 'actividad',
        tipo: 'set',
        datos: datos,
        token: sessionStorage.token
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve({});
      });

      return defer.promise;
    }

    function _getActividad(seccion) {

      var defer = $q.defer();

      $http.post(api, {
        accion: 'actividad',
        tipo: 'get',
        seccion: seccion || '',
        token: sessionStorage.token
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve({});
      });

      return defer.promise;
    }

    function _updateActividad(datos) {

      var defer = $q.defer();

      $http.post(api, {
        accion: 'actividad',
        tipo: 'update',
        datos: datos,
        token: sessionStorage.token
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve({});
      });

      return defer.promise;
    }

    function _sugerencia(email, texto) {

      var defer = $q.defer();

      $http.post(api, {
        accion: 'sugerencias',
        email: email,
        texto: texto,
        token: sessionStorage.token
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve([]);
      });

      return defer.promise;
    }

    function _restablecer(email) {

      var defer = $q.defer();

      $http.post(api, {
        accion: 'restablecer',
        tipo: 'set',
        datos: {
          email: email
        }
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve([]);
      });

      return defer.promise;
    }

    return {
      login: _login,
      registro: _registro,
      getNiveles: _getNiveles,
      getGrupos: _getGrupos,
      getTerminos: _getTerminos,
      getAyuda: _getAyuda,
      getAgradecimientos: _getAgradecimientos,
      getAcerca: _getAcerca,
      buscar: _buscar,
      actividad: _actividad,
      getActividad: _getActividad,
      updateActividad: _updateActividad,
      sugerencia: _sugerencia,
      restablecer: _restablecer

    };
  });


