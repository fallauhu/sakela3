angular.module('sakela')

  .service('Actividad', function($http, $log, $q, lodash, API) {

    var api,
        actividad = {},
        aprendidas = {},
        enProgreso = {},
        pendientes = {};

    api = 'http://hiztegia.bilbozaharra.eus/api.php';

    function _get() {

      var defer = $q.defer();

      API.getActividad().then(function(act) {

        var aciertos = {},
            fallos = {};

        actividad = {};

        lodash.each(act, function(a) {

          if (a.resultado !== 'correcta' && a.resultado !== 'incorrecta') {
            return;
          }

          if (!aciertos[a.nivel]) {
            aciertos[a.nivel] = {};
            fallos[a.nivel] = {};
          }

          if (!aciertos[a.nivel][a.grupo_id]) {

            aciertos[a.nivel][a.grupo_id] = {
              cuantos: 0
            };

            fallos[a.nivel][a.grupo_id] = {};
          }

          if (!aciertos[a.nivel][a.grupo_id][a.termino_id]) {
            aciertos[a.nivel][a.grupo_id][a.termino_id] = 0;
            fallos[a.nivel][a.grupo_id][a.termino_id] = 0;
          }

          if (aciertos[a.nivel][a.grupo_id][a.termino_id] === 3) {
            return;
          }

          if (a.resultado === 'correcta') {
            aciertos[a.nivel][a.grupo_id][a.termino_id] += 1;
          }
          else if (a.resultado === 'incorrecta') {
            aciertos[a.nivel][a.grupo_id][a.termino_id] = 0;
            fallos[a.nivel][a.grupo_id][a.termino_id] += 1;
          }
        });

        lodash.each(aciertos, function(ac, n) {

          if (!actividad[n]) {
            actividad[n] = {
              cuantos: 0
            };
          }

          lodash.each(ac, function(c, g) {

            if (!actividad[n][g]) {
              actividad[n][g] = 0;
            }

            lodash.each(c, function(t, i) {
              if (t === 3 || (t === 1 && fallos[n][g][i] === 0)) {
                actividad[n].cuantos += 1;
                actividad[n][g] += 1;
              }
            });
          });

        });

        defer.resolve(actividad);
      }, function() {
        defer.resolve(actividad);
      });

      return defer.promise;
    }

    function _setAprendida(termino) {

      var i, l = 3;

      for (i = 0; i < l; i += 1) {

        API.actividad({
          seccion: 'progreso',
          termino: termino,
          grupo: {id: termino.grupo_id},
          nivel: termino.nivel,
          resultado: 'correcta'
        });
      }
    }

    function _setEnProgreso(termino) {

      API.updateActividad({
        termino: termino,
        resultado: 'incorrecta'
      });
    }

    function _setPendiente(termino) {

      API.updateActividad({
        termino: termino,
        resultado: 'no_reconocida'
      });
    }

    function _getAprendidasNumero(datos) {

      var defer = $q.defer();

      datos = datos || {};

      $http.post(api, {
        accion: 'actividad',
        tipo: 'get_aprendidas_numero',
        nivel: datos.nivel || null,
        grupo: datos.grupo ? datos.grupo.id : null,
        token: sessionStorage.token
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve({});
      });

      return defer.promise;

    }

    function _getProgreso(nivel) {

      var defer = $q.defer();

      $http.post(api, {
        accion: 'actividad',
        tipo: 'get_progreso',
        nivel: nivel,
        token: sessionStorage.token
      }).then(function(res) {
        defer.resolve(res.data);
      }, function() {
        defer.resolve({});
      });

      return defer.promise;

    }

    return {
      get: _get,
      getAprendidasNumero: _getAprendidasNumero,
      getProgreso: _getProgreso,
      setAprendida: _setAprendida,
      setEnProgreso: _setEnProgreso,
      setPendiente: _setPendiente
    };
  });
