angular.module('sakela')

  .service('Favoritos', function ($log, $q, lodash, API) {

    function _get(id) {

      var defer = $q.defer(),
          favoritos = [];

      API.getActividad('favoritos').then(function(actividad) {

        var terminos = {},
            activos = {};

        favoritos = [];

        if (id) {
          actividad = actividad.filter(function(a) {
            return +(a.termino_id) === id;
          });
        }

        lodash.each(actividad, function(a) {

          if (!activos[a.termino_id]) {
            activos[a.termino_id] = 0;
          }

          if (!terminos[a.termino_id]) {
            terminos[a.termino_id] = a.termino;
          }

          activos[a.termino_id] = a.resultado === 'activo' ? 1 : 0;
        });

        lodash.each(activos, function(t, i) {
          if (t === 1) {
            favoritos.push(terminos[i]);
          }
        });

        defer.resolve(favoritos);
      });

      return defer.promise;

    }

    function _set(termino) {

      return API.actividad({
        seccion: 'favoritos',
        termino: termino,
        resultado: 'activo'
      });
    }

    function _del(termino) {

      return API.actividad({
        seccion: 'favoritos',
        termino: termino,
        resultado: 'desactivado'
      });
    }

    return {
      get: _get,
      set: _set,
      del: _del
    };
  });
