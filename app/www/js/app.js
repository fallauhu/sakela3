angular.module('sakela', [
  'ionic',
  'pascalprecht.translate',
  'ionic.contrib.NativeDrawer',
  'ion-sticky',
  'ngCordova',
  'ngCordovaOauth',
  'angular-svg-round-progressbar',
  'ngLodash',
  'pouchdb',
  'templates'
])

.run(function($log, $rootScope, $ionicPlatform, Diccionario) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    if (navigator.splashscreen) {
      navigator.splashscreen.hide();
    }

    //Diccionario.init();

  });

  $rootScope.$on('$stateChangeSuccess', function(e, toState) {

    $log.log(toState);

  });
})

.config(function($logProvider, $stateProvider, $urlRouterProvider, $translateProvider, $compileProvider) {

  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'menu.html',
    controller: 'AppCtrl',
    controllerAs: 'app'
  })

  .state('app.inicio', {
    url: '/inicio',
    views: {
      'menuContent': {
        templateUrl: 'inicio.html',
        controller: 'InicioCtrl',
        controllerAs: 'inicio'
      }
    }
  })

  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      }
    }
  })

  .state('app.nuevo', {
    url: '/nuevo',
    views: {
      'menuContent': {
        templateUrl: 'nuevo.html',
        controller: 'NuevoCtrl',
        controllerAs: 'nuevo'
      }
    }
  })

  .state('app.favoritos', {
    url: '/favoritos',
    views: {
      'menuContent': {
        templateUrl: 'favoritos.html',
        controller: 'FavoritosCtrl',
        controllerAs: 'favoritos'
      }
    }
  })

  .state('app.preferencias', {
    url: '/preferencias',
    views: {
      'menuContent': {
        templateUrl: 'preferencias.html',
        controller: 'PreferenciasCtrl',
        controllerAs: 'preferencias'
      }
    }
  })

  .state('app.ayuda', {
    url: '/ayuda',
    views: {
      'menuContent': {
        templateUrl: 'ayuda.html',
        controller: 'AyudaCtrl',
        controllerAs: 'ayuda'
      }
    }
  })

  .state('app.sugerencias', {
    url: '/sugerencias',
    views: {
      'menuContent': {
        templateUrl: 'sugerencias.html',
        controller: 'SugerenciasCtrl',
        controllerAs: 'sugerencias'
      }
    }
  })

  .state('app.agradecimientos', {
    url: '/agradecimientos',
    views: {
      'menuContent': {
        templateUrl: 'agradecimientos.html',
        controller: 'AgradecimientosCtrl',
        controllerAs: 'agradecimientos'
      }
    }
  })

  .state('app.acerca', {
    url: '/acerca',
    views: {
      'menuContent': {
        templateUrl: 'acerca.html',
        controller: 'AcercaCtrl',
        controllerAs: 'acerca'
      }
    }
  })

  .state('app.indice', {
    url: '/indice',
    abstract: true,
    views: {
      'menuContent': {
        templateUrl: 'tabs.html',
        controller: 'IndiceCtrl',
        controllerAs: 'indice'
      }
    }
  })

  .state('app.indice.ejercicios', {
    url: '/ejercicios',
    views: {
      'indice-ejercicios': {
        templateUrl: 'ejercicios.html',
        controller: 'EjerciciosCtrl',
        controllerAs: 'ejercicios'
      }
    }
  })

  .state('app.indice.diccionario', {
    url: '/diccionario',
    views: {
      'indice-diccionario': {
        templateUrl: 'diccionario.html',
        controller: 'DiccionarioCtrl',
        controllerAs: 'diccionario'
      }
    }
  })

  .state('app.indice.progreso', {
    url: '/progreso',
    views: {
      'indice-progreso': {
        templateUrl: 'progreso.html',
        controller: 'ProgresoCtrl',
        controllerAs: 'progreso'
      }
    }
  });

  if (localStorage.token && localStorage.usuario && localStorage.email) {

    sessionStorage.token = localStorage.token;
    sessionStorage.usuario = localStorage.usuario;
    sessionStorage.email = localStorage.email;
  }
  else {
    delete sessionStorage.token;
  }

  $urlRouterProvider.otherwise(sessionStorage.token ? '/app/indice/ejercicios' : '/app/inicio');

  $translateProvider.translations('es', {
    INICIO: 'Inicio',
    REGISTRO: 'Registro',
    EMPEZAR: 'Empezar',
    T1: 'Aprendizaje de vocabulario en euskera de forma inteligente',
    T2: 'Aprende vocabulario según el nivel que estés estudiando',
    T3: 'Sakela recuerda lo que ya has aprendido y lo que tienes pendiente',
    T4: 'Diccionario offline superveloz con enlaces a diccionarios online',
    T5: 'Ya tengo una cuenta',
    USUARIO_CORREO: 'Usuario o correo',
    USUARIO: 'Usuario',
    CORREO: 'Correo',
    CONTRASENA: 'Contraseña',
    INGRESAR: 'Ingresar',
    RESTABLECER: 'Restablecer contraseña',
    NUEVO: 'Nuevo usuario',
    REGISTRARSE: 'Registrarse',
    PAGINA_PRINCIPAL: 'Página principal',
    FAVORITOS: 'Favoritos',
    PREFERENCIAS: 'Preferencias',
    SUGERENCIAS: 'Sugerencias',
    RECOMIENDANOS: 'Recomiéndanos',
    AYUDA: 'Ayuda',
    AGRADECIMIENTOS: 'Agradecimientos',
    DE: 'De',
    TEXTO_SUGERENCIAS: 'Si deseas comunicarnos un error, sugerirnos un cambio, facilitarnos algún término que falta en el diccionario...',
    EJERCICIOS: 'EJERCICIOS',
    DICCIONARIO: 'DICCIONARIO',
    PROGRESO: 'PROGRESO',
    BUSCAR: 'Buscar',
    EU_ES: 'Euskera | Castellano',
    ES_EU: 'Castellano | Euskera',
    ESCRIBIR: 'ESCRIBIR',
    SELECCIONAR: 'SELECCIONAR',
    ORTOGRAFIA: 'ORTOGRAFÍA',
    COMPROBAR: 'Comprobar',
    NO_CONOCIDO: 'No conocido',
    RESPUESTA_CORRECTA: 'Respuesta correcta',
    INCORRECTO: 'Incorrecto',
    CORRECTO: 'Correcto',
    CONTINUAR: 'Continuar',
    RESPONDIDO: 'Has respondido correctamente',
    PREGUNTAS: 'preguntas de',
    REPETIR: 'Repetir',
    ACEPTAR: 'Aceptar',
    AMPLIAR: 'Ampliar información en',
    LENGUAJE: 'Lenguaje de la interfaz',
    EUSKARA: 'Euskera',
    CASTELLANO: 'Castellano',
    OFFLINE: 'Usar diccionario offline',
    OFFLINE_TEXTO: 'El diccionario se podrá usar sin acceso a internet',
    APRENDIDAS: 'Aprendidas',
    EN_PROGRESO: 'En progreso',
    PENDIENTES: 'Pendientes',
    COMPLETO: 'Ejercicio finalizado con éxito',
    ACERCA: 'Acerca de'
  });

  $translateProvider.translations('eu', {
    INICIO: 'Hasiera',
    REGISTRO: 'Erregistroa',
    EMPEZAR: 'Hasi',
    T1: 'Euskarazko hiztegia modu adimentsuan ikastea',
    T2: 'Ikasi hiztegia zure euskara mailaren arabera',
    T3: 'Sakela-k dagoeneko zer ikasi duzun eta ikasteko zer duzun gogorarazten du',
    T4: 'Offline hiztegi azkar-azkarra, online hiztegietarako estekak dituena',
    T5: 'Badut kontu bat',
    USUARIO_CORREO: 'Erabiltzailea edo posta-helbidea',
    USUARIO: 'Erabiltzailea',
    CORREO: 'Posta-helbidea',
    CONTRASENA: 'Pasahitza',
    INGRESAR: 'Sartu',
    RESTABLECER: 'Pasahitza berrezarri',
    NUEVO: 'Erabiltzaile berria',
    REGISTRARSE: 'Erregistratu',
    PAGINA_PRINCIPAL: 'Orrialde nagusia',
    FAVORITOS: 'Gustukoenak',
    PREFERENCIAS: 'Lehenespenak',
    SUGERENCIAS: 'Iradokizunak',
    RECOMIENDANOS: 'Gomenda gaitzazu',
    AYUDA: 'Laguntza',
    AGRADECIMIENTOS: 'Esker onak',
    DE: 'Nork',
    TEXTO_SUGERENCIAS: 'Akats bat jakinarazi, aldaketaren bat iradoki, hiztegian falta den terminoren bat eman nahi badiguzu...',
    EJERCICIOS: 'ARIKETAK',
    DICCIONARIO: 'HIZTEGIA',
    PROGRESO: 'AURRERAPENA',
    BUSCAR: 'Bilatu',
    EU_ES: 'Euskara | Gaztelania',
    ES_EU: 'Gaztelania | Euskara',
    ESCRIBIR: 'IDATZI',
    SELECCIONAR: 'HAUTATU',
    ORTOGRAFIA: 'ORTOGRAFIA',
    COMPROBAR: 'Egiaztatu',
    NO_CONOCIDO: 'Ez ezaguna',
    RESPUESTA_CORRECTA: 'Erantzun zuzena',
    INCORRECTO: 'Ez da zuzena',
    CORRECTO: 'Zuzena',
    CONTINUAR: 'Jarraitu',
    RESPONDIDO: 'Zuzen erantzun duzu',
    PREGUNTAS: 'galderak',
    REPETIR: 'Errepikatu',
    ACEPTAR: 'Onartu',
    AMPLIAR: 'Informazio gehiago hemen:',
    LENGUAJE: 'Interfazearen hizkuntza',
    EUSKARA: 'Euskara',
    CASTELLANO: 'Gaztelania',
    OFFLINE: 'Offline hiztegia erabili',
    OFFLINE_TEXTO: 'Interneten sartu gabe erabili ahalko da hiztegia',
    APRENDIDAS: 'Ikasiak',
    EN_PROGRESO: 'Ikasten',
    PENDIENTES: 'Ikasteke',
    COMPLETO: 'Ariketa amaitua arrakastaz',
    ACERCA: 'Honi buruz',
  });

  $translateProvider.useSanitizeValueStrategy('sanitizeParameters');

  $translateProvider.preferredLanguage(localStorage.lang || 'es');

  if (window.cordova) {
     $compileProvider.debugInfoEnabled(false);
     $logProvider.debugEnabled(false);
  }

})

.filter('trusted', ['$sce', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}])


.directive('focusMe', function($timeout) {
  return {
    link: function(scope, element, attrs) {
      scope.$watch(attrs.focusMe, function(value) {
        if(value === true) {
          $timeout(function() {
            element[0].focus();
            scope[attrs.focusMe] = false;
          }, 150);
        }
      });
    }
  };
});
